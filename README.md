# Creating interactive treaty participation maps

This project develops interactive visualizations of treaty participation data for research and education using the free and open source JavaScript library [D3.js](https://d3js.org/) -- an extremely versatile and rich data visualization framework for web publications.

The first output of this project is a map with a time slider underneath allowing one to view annual snapshots of treaty participation, with a tooltip showing signature, consent and entry into force dates for each state on mouseover or touch. Zoom and pan functions are added to show even the smallest states. Here's a screenshot of a zoomed out sample map with the time slider set to 2005:

![screenshotMap](public/img/POPconvScreenshot2005.png "POP Convention participation in 2005")

A minimalist web page containing this map is `single-map.html` in the 'public' folder with its associated stylesheet, data and javascript code. It is live at https://legalinformatics.gitlab.io/treaty-participation-maps/single-map.html

These kinds of maps depend on accurate treaty participation data and suitable map data, and the two need to use a common identifier so as to link the right data with the right country on the map.

More background and details will be described at https://martinakunz.gitlab.io/treaty-analytics/ -- what follows is just the short version to make this repository as self-contained as possible.

## About the data 
The **participation data** source used for this map is the sample CSV output `UNTSparticPOPconv.csv` of a SPARQL query run on the [PILO](https://gitlab.com/legalinformatics/pilo) ontology populated by the [UNTS-treatyrecords-IE](https://gitlab.com/legalinformatics/unts-treatyrecords-ie) app which processes UNTS treaty records harvested with the [UNTS crawler](https://gitlab.com/legalinformatics/unts-crawler). However, as country names are not disambiguated upstream (yet), some preprocessing is almost always necessary. For instance, in the case of the Stockholm Convention, Venezuela and Iran are designated by more than one name in the [source](https://treaties.un.org/Pages/showDetails.aspx?objid=08000002800536cc&clang=_en) and thus generate different treaty participant instances in the PILO database. Additionally, there are two ratification instances for Venezuela, the more recent of which is actually about ratification of amendments as can be seen in the [action details](https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002804c360b&clang=_en). Our preprocessing step needs to disregard the later date, because we are mapping participation in the original treaty, not in amending agreements. 

As for **map data**, this initial version uses the default sovereignty map with moderately detailed (1:50m) boundary lines from [Natural Earth](https://www.naturalearthdata.com/downloads/50m-cultural-vectors/50m-admin-0-details/), a collaborative public domain cartography project. The default boundaries have a distinct American outlook (e.g. Palestine is missing), but they describe their [policy](https://www.naturalearthdata.com/about/disputed-boundaries-policy/) and provide map data for disputed areas for [download](https://www.naturalearthdata.com/downloads/50m-cultural-vectors/50m-admin-0-breakaway-disputed-areas/). In the above-mentioned example, Palestine did acceed to the Convention in 2017, but the map unfortunately cannot show this due to the default boundaries. Upgrading the map data to a version that can represent all treaty participants is a fairly high priority for this project and I could use help with this. Supranational treaty participants is another issue to address in this category (e.g. the EU is party to the Stockholm Convention).

Specifically, after downloading the `ne_50m_admin_0_sovereignty.zip` file from the linked [page](https://www.naturalearthdata.com/downloads/50m-cultural-vectors/50m-admin-0-details/), the shapefile in the extracted archive first needs to be converted to GeoJSON format. This can be done with the [`ogr2ogr`](https://gdal.org/programs/ogr2ogr.html) command line tool using the following command:

```
ogr2ogr -f GeoJSON -select "NAME,NAME_AR,NAME_EN,NAME_ES,NAME_FR,NAME_RU,NAME_ZH" -lco ID_FIELD=NAME -lco COORDINATE_PRECISION=5 ne_50m_admin_0_sovereignty_v511_names.geojson ne_50m_admin_0_sovereignty.shp
```
To limit file size, only the six official UN languages are chosen among the many attributes contained in Natural Earth data and coordinate precision is reduced to 5 decimals (this makes a difference of about 2MB).

The names need to be streamlined and prepared for display in the map tooltips, which means shortening them without losing readability while staying as close as possible to the official names used in UN documents. The file `data-processing.org` contains the conversion tables, Python code and results for the Stockholm Convention. The file is plain text and can be opened in any text editor, but is best viewed in  [Emacs Orgmode](https://orgmode.org/) which is installed as standard on many GNU Linux and Mac OS systems.

The prerequisites for running the code are:
1. Install or update [GNU Emacs](https://www.gnu.org/software/emacs/download.html)) -- I have Emacs 27.1 with Orgmode 9.5, but probably anything from Emacs 24 with Orgmode 8 would work
2. Install or update [Python](https://www.python.org/downloads/) -- I have version 3.9 but any version >= 3.5 would probably work
3. Enable Orgmode support for Python in your Emacs configuration file, adding it to the list of languages to load (see [instructions](https://orgmode.org/worg/org-contrib/babel/languages/ob-doc-python.html))
4. Install the Python data science package [`pandas`](https://pandas.pydata.org/pandas-docs/stable/getting_started/install.html) 

The resulting files (`ne_50m_admin_0_sovereignty_v511_names.geojson` and `UNTSparticPOPconv_edited.csv`) are in the 'data' folder of the 'public' directory of this repository.


## Map projection
The map projection used in the sample map above is the Equal Earth map projection by Šavrič et al.[^1] as implemented in [D3](https://observablehq.com/@d3/equal-earth). There are many more map projections to choose from in [`d3-geo`](https://github.com/d3/d3-geo), but a map projection preserving true proportions of land areas would definitely be my recommendation for a global treaty participation map. (I find it surprising that a number of international organizations still use the Mercator projection with a setting that makes Greenland appear nearly as large as Africa when in reality Africa is about 14 times larger than Greenland.)

Of course an orthographic projection (globe) is more visually appealing, but not being able to see all continents at a glance does have some downsides, especially for this kind of spatiotemporal map with a time slider. For illustration, this is a screenshot of an orthographic projection of the same sample map set to a different year and centered on a different continent/country:

![screenshotGlobe](public/img/POPconvScreenshotGlobe.png "POP Convention participation in 2020")

The web page `index.html` in the 'public' folder contain both the flat map and the globe projection and can be seen in action at https://legalinformatics.gitlab.io/treaty-participation-maps/


## Customization
The two HTML files are intended for download and customization and are deliberately bare-bones. Once downloaded with their associated stylesheet, data and code, they can be edited with any text editor (I'd recommend Emacs) and  displayed in your browser locally (not on the internet).

If you're new to this, this directory can be downloaded on the command line with
```
git clone https://gitlab.com/legalinformatics/treaty-participation-maps.git
```
or downloaded as an archive and extracted to a suitable location.

There are two ways to display them, one is by opening the file directly (the address bar in the browser will show something like 'file://...single-map.html') and the other is by launching a local web server.

The first option may need temporarily disabling a security policy which does not allow loading javascript code in this way, trowing a [CORS error](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS/Errors/CORSRequestNotHttp). In Firefox, this policy can be disabled by entering `about:config` in the address bar, searching for `security.fileuri.strict_origin_policy` and setting it to `false` (this should be reverted to the default afterwards). Older versions or other browsers may not throw this error, so you try it out first and if the map doesn't appear on the page check the web console (right-click anywhere on the page and select 'Inspect', then switching to 'Console' tab to see error messages).

For the second option I would recommend using a simple Python web server if you don't already have a web server running. [`http.server`](https://docs.python.org/3/library/http.server.html) is a standard module and can be started from the command line as follows:

```
cd targetDir
python3 -m http.server
```
with targetDir being the path to the directory with the web pages you wish to display (in my case this is '~/git/treaty-participation-maps/public', as an example).


## Limitations
To recap the main caveats:
- UNTS participation data may not be accurate and up-to-date
- Withdrawal, suspension, exclusion and termination are not implemented yet
- The data preprocessing code changes historical and full state names to short contemporary ones, coming as close as possible to official treaty participation data
- Data preprocessing needs to be customized or at least double-checked for each treaty due to quirks in upstream data and code
- Borders on the map are based on the default sovereignty data by Natural Earth and do not imply endorsement


## Notes
[^1]: Šavrič, B., Patterson, T., and Jenny, B. (2018), "The Equal Earth map projection" *International Journal of Geographical Information Science*, 33(3), p. 454-465. https://doi.org/10.1080/13658816.2018.1504949
